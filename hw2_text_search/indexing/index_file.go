package indexing

import (
	"bufio"
	"fmt"
	"os"
	"sync"
)

var Words = make(map[string]int, 0)

var mu = sync.Mutex{}

func IndexFile(wg *sync.WaitGroup, filename string) {
	defer wg.Done()
	file, err := os.Open(filename)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Cannot open file: " + filename)
		return
	}
	scan := bufio.NewScanner(file)
	scan.Split(bufio.ScanWords)
	for scan.Scan() {
		word := scan.Text()
		mu.Lock()
		Words[word] = Words[word] + 1
		mu.Unlock()
	}
}
