package main

import (
	"./indexing"
	"bufio"
	"fmt"
	"os"
	"sort"
	"sync"
)

type Pair struct {
	Word  string
	Count int
}

func (p Pair) String() string {
	return fmt.Sprintf("%s => %d", p.Word, p.Count)
}

func main() {
	scan := bufio.NewScanner(os.Stdin)
	scan.Split(bufio.ScanWords)
	wg := sync.WaitGroup{}
	for scan.Scan() {
		wg.Add(1)
		go indexing.IndexFile(&wg, scan.Text())
	}
	wg.Wait()

	wordList := make([]Pair, len(indexing.Words))
	i := 0
	for k, v := range indexing.Words {
		wordList[i] = Pair{k, v}
		i++
	}
	sort.Slice(wordList, func(i, j int) bool {
		return wordList[i].Count > wordList[j].Count
	})

	size := len(indexing.Words);
	fmt.Printf("Число уникальных слов: %d\n", size)
	for i := 0; i < size && i < 20; i++ {
		fmt.Println(wordList[i])
	}
}
