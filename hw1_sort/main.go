package main

import (
	"./mergesort"
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	file, err := os.Open("stdin")
	if err != nil {
		os.Exit(1)
	}
	scan := bufio.NewScanner(file)
	scan.Split(bufio.ScanWords)
	var values []int
	for scan.Scan() {
		value, err := strconv.Atoi(scan.Text())
		if err != nil {
			os.Exit(1)
		}
		values = append(values, value)
	}
	fmt.Printf("%v", mergesort.Sort(values))
}
