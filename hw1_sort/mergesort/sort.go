package mergesort

func Sort(values []int) []int {
	if len(values) == 1 {
		return values
	}
	pivot := len(values) / 2
	return merge(
		Sort(values[:pivot]),
		Sort(values[pivot:]),
	)
}

func merge(left, right []int) []int {
	size := len(left) + len(right)
	values := make([]int, size, size)
	for i, j, k := 0, 0, 0; k < size; k++ {
		if i > len(left)-1 && j <= len(right)-1 {
			values[k] = right[j]
			j++
		} else if j > len(right)-1 && i <= len(left)-1 {
			values[k] = left[i]
			i++
		} else if left[i] < right[j] {
			values[k] = left[i]
			i++
		} else {
			values[k] = right[j]
			j++
		}
	}
	return values
}
