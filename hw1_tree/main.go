package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
)

func filterDirs(files []os.FileInfo) []os.FileInfo {
	res := make([]os.FileInfo, 0)
	for _, f := range files {
		if f.IsDir() {
			res = append(res, f)
		}
	}
	return res
}

func getFiles(dir *os.File, printFiles bool) ([]string, error) {
	res := make([]string, 0)
	files, err := dir.Readdir(-1)
	if err != nil {
		return res, err
	}
	if !printFiles {
		files = filterDirs(files)
	}
	for _, f := range files {
		res = append(res, f.Name())
	}
	sort.Strings(res)
	return res, nil
}

func printFile(out io.Writer, path string, prefix string, printFiles bool, isLast bool) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()
	info, err := file.Stat()
	if err != nil {
		return err
	}
	if info.IsDir() || printFiles {
		sep := "├"
		if isLast {
			sep = "└"
		}
		size := ""
		if !info.IsDir() && printFiles {
			if info.Size() > 0 {
				size = fmt.Sprintf(" (%db)", info.Size())
			} else {
				size = " (empty)"
			}
		}
		fmt.Fprintf(out, "%s%s───%s%s\n", prefix, sep, info.Name(), size)
	}
	if info.IsDir() {
		ownPrefix := "│"
		if isLast {
			ownPrefix = ""
		}
		files, err := getFiles(file, printFiles)
		if err != nil {
			return err
		}
		for i, f := range files {
			fPath := filepath.Join(file.Name(), f)
			fPrefix := prefix + ownPrefix + "\t"
			fIsLast := i == len(files)-1
			printFile(out, fPath, fPrefix, printFiles, fIsLast)
		}
	}
	return nil
}

func dirTree(out io.Writer, path string, printFiles bool) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()
	files, err := getFiles(file, printFiles)
	if err != nil {
		return err
	}
	for i, f := range files {
		printFile(out, filepath.Join(file.Name(), f), "", printFiles, i == len(files)-1)
	}
	return nil
}

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}
